from django.conf.urls import url
from Users import views

from django.conf.urls.static import static
from django.conf import settings

from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    url(r'^department$', views.departmentApi),
    url(r'^department/([0-9]+)$', views.departmentApi),
    url(r'^user$', views.usersApi),
    url(r'^user/([0-9]+)$', views.usersApi),
    url(r'^user/savefile', views.SaveFile),
    url(r'^token', obtain_auth_token, name='api_token_auth'),
    url(r'^newuser', views.newuserApi),
    
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
