from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from Users.models import Departments, Users
from Users.serializers import DepartmentsSerializer, UsersSerializers, UserSerializer

from django.core.files.storage import default_storage

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
# Create your views here.



@csrf_exempt
@api_view(['GET','POST'])
@permission_classes([IsAuthenticated])
def departmentApi(request,id=0):    
    if request.method=='GET':
        departments = Departments.objects.all()
        departments_serializer = DepartmentsSerializer(departments, many=True)
        return JsonResponse(departments_serializer.data, safe=False)
    elif request.method=='POST':
        department_data=JSONParser().parse(request)
        departments_serializer=DepartmentsSerializer(data=department_data)
        if departments_serializer.is_valid():
            departments_serializer.save()
            return JsonResponse("Added Succesfully", safe=False)
        return JsonResponse("Failed to Add", safe=False)
    elif request.method=='PUT':
        department_data=JSONParser().parse(request)
        department=Departments.objects.get(DepartmetId=department_data['DepartmentId'])
        departments_serializer=DepartmentsSerializer(department,data=department_data)
        if departments_serializer.is_valid():
            departments_serializer.save()
            return JsonResponse("Updated Successfully", safe=False)
        return JsonResponse('Fails to Update')
    elif request.method=='DELETE':
        department=Departments.objects.get(DepartmentId=id)
        department.delate()
        return JsonResponse('Deleted Successfully', safe=False)

@csrf_exempt
@api_view(['GET','POST','PUT','DELETE'])
@permission_classes([IsAuthenticated])
def usersApi(request,id=0):
    if request.method=='GET':
        users = Users.objects.all()
        users_serializer = UsersSerializers(users, many=True)
        return JsonResponse(users_serializer.data, safe=False)
    elif request.method=='POST':
        user_data=JSONParser().parse(request)
        users_serializer=UsersSerializers(data=user_data)
        if users_serializer.is_valid():
            users_serializer.save()
            return JsonResponse("Added Succesfully", safe=False)
        return JsonResponse("Failed to Add", safe=False)
    elif request.method=='PUT':
        user_data=JSONParser().parse(request)
        user=Users.objects.get(UserId=user_data['UserId'])
        users_serializer=UsersSerializers(user,data=user_data)
        if users_serializer.is_valid():
            users_serializer.save()
            return JsonResponse("Updated Successfully", safe=False)
        return JsonResponse('Fails to Update')
    elif request.method=='DELETE':
        user=Users.objects.get(UserId=id)
        user.delate()
        return JsonResponse('Deleted Successfully', safe=False)

@csrf_exempt
def SaveFile(request):
    file=request.FILES['file']
    file_name=default_storage.save(file.name,file)
    return JsonResponse(file_name,safe=False)

@csrf_exempt
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def newuserApi(request,id=0):
    if request.method=='POST':
        user_data=JSONParser().parse(request)
        users_serializer=UserSerializer(data=user_data)
        if users_serializer.is_valid():
            users_serializer.save()
            return JsonResponse("Added Succesfully", safe=False)
        return JsonResponse("Failed to Add", safe=False)
    else:
        return JsonResponse("Failed to Add New User", safe=False)